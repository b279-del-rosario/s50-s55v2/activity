import './App.css';
import AppNavbar from "./components/AppNavbar";
/*import Banner from "./components/Banner"
import Highlights from "./components/Highlights"*/
import Home from "./pages/Home"
import Courses from "./pages/Courses"
import Register from "./pages/Register"
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from "./pages/Error"

// npm install react-router-dom
import { Route, Routes } from "react-router-dom"
import { BrowserRouter as Router } from "react-router-dom"
import { Container } from "react-bootstrap";
import { useState } from "react"
import { UserProvider } from "./UserContext"

// React JS is a single page application (SPA)
// Whenever a link is clicked, it functions as if the page is being reloaded but what it actually does is it goes through the process of rendering, mounting, rerendering and unmounting components
// When a link is clicked, React JS changes the url of the application to mirror how HTML accesses its urls
// It renders the component executing the function component and it's expressions
// After rendering it mounts the component displaying the elements
// Whenever a state is updated or changes are made with React JS, it rerenders the component
// Lastly, when a different page is loaded, it unmounts the component and repeats this process
// The updating of the user interface closely mirrors that of how HTML deals with page navigation with the exception that React JS does not reload the whole page

function App() {

  // Creating a user state for global scope
  const [user, setUser] = useState({
    email: localStorage.getItem("email")
  })

  // Function for clearing the storage on logout
  const unsetUser = () => {
    localStorage.clear();
  }



  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
          <Container>
              <Routes>
                  <Route path="/" element={<Home/>}/>
                  <Route path="/courses" element={<Courses/>}/>
                  <Route path="/register" element={<Register/>}/>
                  <Route path="/login" element={<Login/>}/>
                  <Route path="/logout" element={<Logout/>}/>
                  <Route path="*" element={<Error/>}/>
              </Routes>
          </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
