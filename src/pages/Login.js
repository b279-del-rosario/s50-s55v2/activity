import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from "../UserContext"
import { Navigate } from "react-router-dom";

export default function Login() {
    const { user, setUser } = useContext(UserContext);


    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    }, [email, password]);

    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        setUser({
            // Store the email in the localStroge
            email: localStorage.setItem("email", email)
        })

        // Clear input fields after submission
        setEmail('');
        setPassword('');

        alert(`${email} has been verified! Welcome back!`);

    }





    return (
        (user.email !== null) ?
            <Navigate to="/courses"/>
        :
        <>
            <h1>Login Page</h1>
            <Form onSubmit={(e) => authenticate(e)} className="my-5">
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    required
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    required
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            { isActive ? 
                <Button variant="success" type="submit" id="submitBtn" className="mt-3">
                    Login
                </Button>
                : 
                <Button variant="danger" type="submit" id="submitBtn" className="mt-3" disabled>
                    Login
                </Button>
            }

        </Form>
        </>
        
    )
}
