import courseData from "../data/courses"
import CourseCard from '../components/CourseCard'

export default function Courses(){
	// Checks to see if mock data was captured
	console.log(courseData);
	console.log(courseData[0]);

	const courses = courseData.map(course => {
		return(
			<CourseCard key={course.id} courseProp={course}/>
			);
	})

	return(
		<>
		<h1>Courses</h1>
		{/*Prop making and prop passing*/}
		{courses}
		</>
		)
}