import { Card, Button } from 'react-bootstrap';
// In React.js we have 3 hooks
/*
1. useState
2. useEffect
3. useContext
*/

import { useState } from "react"

export default function CourseCard({courseProp}) {
    // Checks if props was successfully passed
    console.log(courseProp.name);
    // Checks the type of the passed data
    console.log(typeof courseProp);

    // Destructuring the courseProp into their own variables
    const {name, description, price } = courseProp;

    // useState
    // Used for storing different states
    // used to keep track of information to individual components

    // SYNTAX -> const [getter, setter] = useState(initialGetterValue);

    const [count, setCount] = useState(0);
    console.log(useState(0));

    const [seat, setSeat] = useState(30);
     


    // Function that keeps track of the enrollees for a course
    function enroll(){
        if(count >= 30 && seat <= 0 ){
            setCount(count + 0);
            setSeat(seat - 0);
            return alert("No More Seat Available");
        }else{
            setCount(count + 1);
            setSeat(seat - 1);
        }

    }
    
    return (

        <Card className="my-3">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                 <Card.Subtitle>Enrollees:</Card.Subtitle>
                <Card.Text>{count}</Card.Text>
                <Button variant="primary" onClick={enroll}>Enroll</Button>
            </Card.Body>
        </Card>
    )
}
